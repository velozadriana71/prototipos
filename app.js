// class AgedPerson {
//     printAge() {
//         console.log(this.age);
//     }
// }

// class Person extends AgedPerson{
//     name = 'Adri';

//     constructor() {
//         super();
//         this.age = 23;
//     }

//     greet() {
//         console.log(`Hi , I am ${this.name} and I am ${this.age} years old`);
//     }
// };

//Función constructora.
// function Person() {
//   this.age = 23;
//   this.name = "Adri";
//   this.greet = function () {
//     console.log(`Hi , I am ${this.name} and I am ${this.age} years old`);
//   };
// }

// person.prototype = {
//     printAge() {
//         //La palabra this dentro de los prototipos se refiere a su objeto en el que llama al método
//         console.log(this.age);
//     }
// }

// console.dir(Person);

// const person = new Person();
// person.greet();
// //console.log(p.__proto__ === Person.prototype);
// console.log(p.__proto__);
// const p2 = new p.__proto__.constructor();
// console.log(p2)

const course = {
    title: 'Javascript the complete guide',
    rating: 5
}

console.log(Object.getPrototypeOf(course));

//Este toma dos parametros el primero es el objeto donde se  va a configura el prototito
//El segundo es un prototipo que desea usar
Object.setPrototypeOf(course, {
    
    printRating: function() {
        console.log(`${this.rating/5}`)
    }
})

course.printRating();